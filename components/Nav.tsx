import Link from "next/link";
import * as React from "react";

const Nav: React.FunctionComponent = (props) => {
  return (
    <nav className="flex flex-wrap items-center justify-between py-2 bg-gray-900 text-gray-200 shadow-lg navbar navbar-expand-lg navbar-light">
      <div className="container-fluid w-full flex flex-wrap items-center justify-center px-6">
          <ul className="flex">
            <li className="nav-item p-2">
              <Link href="/">
                <a
                  role="link"
                  aria-label="home"
                  className="nav-link text-white opacity-60 hover:opacity-80 focus:opacity-80 p-0"
                >
                  Home
                </a>
              </Link>
            </li>
            <li className="nav-item p-2">
              <Link href="/products">
                <a
                  role="link"
                  aria-label="products"
                  className="nav-link text-white opacity-60 hover:opacity-80 focus:opacity-80 p-0"
                >
                  Product Search
                </a>
              </Link>
            </li>
            <li className="nav-item p-2">
              <Link href="/about">
                <a
                  role="link"
                  aria-label="about"
                  className="nav-link text-white opacity-60 hover:opacity-80 focus:opacity-80 p-0"
                >
                  About
                </a>
              </Link>
            </li>
          </ul>
      </div>
    </nav>
  );
};

export default Nav;

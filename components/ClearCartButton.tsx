import * as React from "react";
import { Product } from "./products/models";
import { useCart } from "react-use-cart";
import { toast } from 'react-nextjs-toast'

interface ClearCartButtonProps {
}

const ClearCartButton: React.FunctionComponent<ClearCartButtonProps> = () => {
  const { clearCartMetadata, emptyCart } = useCart();
  return (
    <button
      onClick={() => {
        emptyCart();
        clearCartMetadata();
        toast.notify(`Cart has been cleared`)
      }}
      className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 border border-red-700 rounded"
    >
      Clear Cart
    </button>
  );
};

export default ClearCartButton;

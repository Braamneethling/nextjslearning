import { useEffect, useState } from "react";
import Image from "next/image";

interface FallbackImageProps {
    src: string;
    alt: string;
}

export const FallbackImage: React.FunctionComponent<FallbackImageProps> = ({ src,alt, ...rest }) => {
  const [imgSrc, setImgSrc] = useState(src);
  useEffect(() => {
    setImgSrc(src);
  }, [src]);
  return (
    <Image
      {...rest}
      width={200}
      height={200}
      src={imgSrc ? imgSrc : "/images/not-found.png"}
      alt="test"
      onError={() => {
        setImgSrc("/images/not-found.png");
      }}
    />
  );
};

import * as React from 'react';

interface HomeBodyProps {
}

const HomeBody: React.FunctionComponent<HomeBodyProps> = (props) => {
  return (
    <div className="flex flex-col justify-center items-center">Welcome to our home, we like cake</div>
  );
};

export default HomeBody;

import * as React from "react";
import { Product } from "./products/models";
import { useCart } from "react-use-cart";
import { toast } from 'react-nextjs-toast'

interface AddToCartButtonProps {
  product: Product;
}

const AddToCartButton: React.FunctionComponent<AddToCartButtonProps> = ({
  product: product,
}) => {
  const { addItem,totalItems } = useCart();
  return (
    <button
      onClick={() => {
        addItem({ id: product.id.toString(), price: Number(product.price) });
        toast.notify(`Product : ${product.title} has been added`)
        console.log(totalItems)
      }}
      className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded"
    >
      Add
    </button>
  );
};

export default AddToCartButton;

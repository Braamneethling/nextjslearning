import * as React from 'react';

interface AboutUsProps {
}

const AboutUs: React.FunctionComponent<AboutUsProps> = (props) => {
  return (
    <div className="flex flex-col justify-center items-center">Something About Us</div>
  );
};

export default AboutUs;

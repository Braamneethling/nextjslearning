import * as React from 'react';
import styles from "../styles/Home.module.css";

interface FooterProps {
}

const Footer: React.FunctionComponent<FooterProps> = (props) => {
  return (
    <footer className={styles.footer}>
        <a href="https://www.chillisoft.co.za">
          Powered By: &nbsp;
          <img
            src="/images/Chillisoft.jpg"
            alt="Chillisoft"
            width="50"
            height="50"
          />
        </a>
      </footer>
  );
};

export default Footer;

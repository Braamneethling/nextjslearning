import * as React from "react";
import { Product } from "./models";
import MyLazyImage from "../MyLazyImage";
import AddToCartButton from "../AddToCartButton";
import ClearCartButton from "../ClearCartButton";

interface ProductSearchResultsProps {
  products: Product[];
}

const ProductSearchResults: React.FunctionComponent<
  ProductSearchResultsProps
> = ({ products }) => {
  return (
    
    <div className="max-w-7xl mx-auto mt-8">
      <ClearCartButton/>
      {products &&
        products.map((product: Product) => {
          
          return (
            <div key={product.id} className="grid grid-cols-6">
              <div className="p-1">{product.id}<AddToCartButton product={product}/></div>
              <div className="p-1">{product.title}</div>
              <div className="p-1">R {product.price}</div>
              <div className="p-1">{product.category}</div>
              <MyLazyImage alt={product.title} src={product.image}/>
            </div>
          );
        })}
    </div>
  );
};

export default ProductSearchResults;

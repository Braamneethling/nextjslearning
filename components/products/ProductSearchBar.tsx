import * as React from "react";
import { ChangeEvent, MouseEventHandler, useEffect, useState } from "react";

interface ProductSearchBarProps {
  onSearchClick: (searchText: string) => void;
}

const ProductSearchBar: React.FunctionComponent<ProductSearchBarProps> = ({
  onSearchClick,
}) => {
  const [searchText, setSearchText] = useState("");
  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };
  const handleSearchClick = () => {
    onSearchClick(searchText);
  };

  return (
    <>
      <div className="flex flex-col justify-center items-center mt-4">
        <div className="flex items-center border-b border-gray-900 m-auto justify-center">
            <input
              className="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
              role="textbox"
              aria-label="searchBox"
              placeholder="Find products..."
              onChange={handleInputChange}
              value={searchText}
            ></input>
            <button
              role="button"
              aria-label="searchButton"
              onClick={handleSearchClick}
              className="flex-shrink-0 bg-gray-900 text-sm border-4 text-white py-1 px-2 hover:opacity-80 focus:opacity-80 p-0"
            >
              Search
            </button>
        </div>
      </div>
    </>
  );
};

export default ProductSearchBar;

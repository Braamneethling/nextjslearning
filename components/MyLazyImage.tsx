import * as React from "react";
import { useState } from "react";
import Image from "next/image";
import { LazyLoadImage } from "react-lazy-load-image-component";

interface MyLazyImageProps {
  src: string;
  alt: string;
}

const MyLazyImage: React.FunctionComponent<MyLazyImageProps> = ({
  src,
  alt,
}) => {
  const [errorImage, setErrorImage] = useState(false);
  const errorImageUrl = "/images/no-image.png";
  const url = errorImage ? errorImageUrl : `${src}`;
  return (
    <>
      <LazyLoadImage
        alt={alt}
        src={url} // use normal <img> attributes as props
        height={200}
        width={200}
        placeholderSrc="/images/placeholder.png"
        onError={() => {
          if (!errorImage) {
            setErrorImage(true);
          }
        }}
      />
      <span>{alt}</span>
    </>
  );
};

export default MyLazyImage;

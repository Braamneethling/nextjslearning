import { Product } from "./../../../components/products/models";

export const getTestProducts = (count: number = 3): Product[] => {
  const testProducts = [];
  for (let i = 0; i < count; i++) {
    const id = i + 1;
    testProducts.push({
      id,
      title: `title${id}`,
      description: `description${id}`,
      image: `image${id}`,
      category: `category${id}`,
      price: `R${id}0`,
    });
  }
  return testProducts;
};

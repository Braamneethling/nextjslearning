import {
  render,
  screen,
  waitFor,
  fireEvent,
  findByRole,
} from "@testing-library/react";
import ProductSearchBar from "../../../components/products/ProductSearchBar";
import userEvent from "@testing-library/user-event";


interface TestSearchBarProps {
  onSearchClick?: (searchText: string) => void;
}

const renderSearchBar = (props?: TestSearchBarProps) => {
  return render(
    <ProductSearchBar
      onSearchClick={props?.onSearchClick ? props.onSearchClick : () => {}}
    />
  );
};

describe("ProductSearchBar", () => {
  it("should have input with placeholder 'Find products...'", () => {
    renderSearchBar();
    const input = screen.getByRole("textbox", {
      name: /searchBox/i,
    }) as HTMLInputElement;

    expect(input.placeholder).toEqual("Find products...");
  });
  it("should have a search button with text 'Search' on it", () => {
    renderSearchBar();
    const button = screen.getByRole("button", {
      name: /searchButton/i,
    }) as HTMLButtonElement;

    expect(button.textContent).toEqual("Search");
  });
  it("should return search text in onSearchClick event when search button clicked", async () => {
    let searchText = "";
    const inputValue = "jacket";
    const props = {
      onSearchClick: (text: string) => {
        searchText = text;
      }
    };
    renderSearchBar(props);
 
    const input = screen.getByRole("textbox", {
      name: /searchBox/i,
    }) as HTMLInputElement;

    const button = screen.getByRole("button", {
      name: /searchButton/i,
    }) as HTMLButtonElement;

    await userEvent.type(input, inputValue);
    await userEvent.click(button);

    await waitFor(() => {
      expect(searchText).toEqual(inputValue);
    });
  });
});

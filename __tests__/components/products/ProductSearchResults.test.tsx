import {
  render,
  screen,
  waitFor,
  fireEvent,
  findByRole,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { getTestProducts } from "./testProducts";
import ProductSearchResults from "../../../components/products/ProductSearchResults";

describe("ProductSearchResults", () => {
  it("Given a 3 products should display all 3 titles", () => {
    const products = getTestProducts(3);
    render(<ProductSearchResults products={products} />);
    const row1 = screen.getByText("title1");
    const row2 = screen.getByText("title2");
    const row3 = screen.getByText("title3");
    expect(row1).not.toBeNull();
    expect(row2).not.toBeNull();
    expect(row3).not.toBeNull();
  });
});

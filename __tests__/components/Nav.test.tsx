import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import Nav from "../../components/Nav";

describe("Nav", () => {
  it("should display home link", () => {
    render(<Nav />);
    const homeLink = screen.getByRole("link", {
      name: /home/i,
    }) as HTMLAnchorElement;

    expect(homeLink.href).toEqual("http://localhost/");
    expect(homeLink).toHaveTextContent("Home");
  });
  it("should display about link", () => {
    render(<Nav />);
    const homeLink = screen.getByRole("link", {
      name: /about/i,
    }) as HTMLAnchorElement;
    
    expect(homeLink.href).toEqual("http://localhost/about");
    expect(homeLink).toHaveTextContent("About");
  });
});

import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import HomeBody from "../../../components/HomeBody";


describe("Nav", () => {
  it("should display home text", () => {
    render(<HomeBody />);
    let expected = "Welcome to our home, we like cake";
    expect(screen.getByText(expected)).toBeInTheDocument();
  });
});

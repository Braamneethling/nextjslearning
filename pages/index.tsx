import type { NextPage } from "next";
import HomeBody from "../components/HomeBody";

const Home: NextPage = () => {
  return (
      <HomeBody/>
  );
};

export default Home;

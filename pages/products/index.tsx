import { NextPage } from "next";
import * as React from "react";
import { useEffect, useState } from "react";
import { Product } from "../../components/products/models";
import ProductSearchBar from "../../components/products/ProductSearchBar";
import ProductSearchResults from "../../components/products/ProductSearchResults";

interface ProductSearchProps {}

const ProductSearch: NextPage<ProductSearchProps> = (props) => {
  const [products, setProducts] = useState([]);
  const [searchText, setSearchText] = useState("");
  useEffect(() => {}, []);

  const handleSearch = (text: string) => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((products) => {
        const filteredProducts = products.filter((product: Product) => {     
          return product.title.includes(text);
        });
        setProducts(filteredProducts);
      });
  };
  return (
    <>
      <ProductSearchBar onSearchClick={handleSearch} />
      <ProductSearchResults products={products} />
    </>
  );
};

export default ProductSearch;

import "../styles/globals.css";
import type { AppProps } from "next/app";
import Nav from "../components/Nav";
import { CartProvider } from "react-use-cart";
import { ToastContainer } from 'react-nextjs-toast'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <CartProvider>
      <ToastContainer />
        <Nav />
        <Component {...pageProps} />
      </CartProvider>
    </>
  );
}

export default MyApp;

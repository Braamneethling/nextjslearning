module.exports = {
  setupFilesAfterEnv: ["./jest.setup.js"],

  moduleNameMapper: {
    "^@components(.*)$": "<rootDir>/components$1",

    "^@pages(.*)$": "<rootDir>/pages$1",

    "^@hooks(.*)$": "<rootDir>/hooks$1",
  },

  testRegex: "/__tests__/.*.(test|spec)\\.(tsx|ts)$",

  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node",
    "svg",
    "png",
    "jpg",
  ],
  testEnvironment: 'jsdom',
};
